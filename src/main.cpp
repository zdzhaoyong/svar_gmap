#include "Map.h"
#include "MapPoint.h"
//#include "MapFrame.h"
#include <GSLAM/core/Jvar.h>

using namespace GSLAM;

MapPtr load(std::string path){
    MapPtr ret=createMap("Hash");
    ret->load(path);
    return ret;
}

REGISTER_SVAR_MODULE(gmap){
    sv::Class<PointID>("size_t")
            .construct<int>()
            .def("__int__",[](GSLAM::PointID id){return (int)id;});

//    sv::Class<MapPoint>("MapPoint")
//            .shared_construct<PointID,Point3Type,Point3Type,pi::Point3ub,GSLAM::FrameID>();

    sv::Class<MapFrame>("MapFrame")
            .def("getParents",[](FramePtr self){
        FrameConnectionMap connections=self->getParents();
        std::vector<sv::Svar> cons;
        for(auto c:connections){
            cons.push_back({{"id",int(c.first)},{"matches",c.second->matchesNum()}});
        }

        return cons;
    })
    .def("getCamera",[](FramePtr self){
        auto camera=self->getCamera();
        return camera.getParameters();
    });

    sv::Class<PointID>("PointID")
            .construct<int>();

    sv::Class<FrameID>("FrameID")
            .construct<int>();

    sv::Class<GSLAM::Map>("Map")
            .def("__init__",[](){
        return createMap("Hash");
    })
            .def("insertMapPoint",&Map::insertMapPoint)
            .def("insertMapFrame",&Map::insertMapFrame)
            .def("eraseMapPoint",&Map::eraseMapPoint)
            .def("eraseMapFrame",&Map::eraseMapFrame)
            .def("clear",&Map::clear)
            .def("frameNum",&Map::frameNum)
            .def("pointNum",&Map::pointNum)
            .def("getFrame",[](Map* self,int id){return self->getFrame(id);})
            .def("getPoint",&Map::getPoint)
            .def("getFrames",(FrameArray(Map::*)()const)&Map::getFrames)
            .def("getPoints",(PointArray(Map::*)()const)&Map::getPoints)
            .def("setLoopDetector",&Map::setLoopDetector)
            .def("getLoopDetector",&Map::getLoopDetector)
            .def("obtainCandidates",(LoopCandidates(Map::*)(const FramePtr&))&Map::obtainCandidates)
            .def("save",&Map::save)
            .def("load",&Map::load)
            .def("getPid",&Map::getPid)
            .def("getFid",&Map::getFid);

    jvar["load"]=load;
}

EXPORT_SVAR_INSTANCE
